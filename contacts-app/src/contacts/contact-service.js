import axios from 'axios'

export default {
  getContacts() {
    return axios.get('http://localhost:3000/contacts')
  },

  createContact(data) {
    return axios.post('http://localhost:3000/contacts', data)
  },

  getContact(id) {
    return axios.get('http://localhost:3000/contacts/' + id)
  }, 

  updateContact(contact) {
    return axios.put('http://localhost:3000/contacts/' + contact.id, contact)
  },
  findContacts(search){
    return axios.get('http://localhost:3000/contacts?q='+ search)
  }
}