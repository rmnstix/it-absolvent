import React from 'react';
import { Link } from 'react-router-dom';
import ROUTES from '../routes'
// import Detail from './Detail';

import contactsService from './contact-service'

class ContactListing extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      contacts: [],
      search: ''
    }
  }

  componentWillMount() {
    this.load()
  }

  async load() {
    const res = await contactsService.getContacts()
      
    this.setState({
        contacts: res.data
      })
  }

  async newLoad(search){
    const res = await contactsService.findContacts(search)

    this.setState({
      contacts: res.data
    })
    console.log(this.state.contacts)
  }

  updateSearch(e) {
    this.setState({
      search: e.target.value
    })
    this.newLoad(this.state.search)
  }

  render() {
    const contacts = this.state.contacts
    // // console.log(this.state.fileteredContacts)
    // console.log(this.state.search)
    return (
      <div>
        <div>
          <span> Search: </span>
          <input type="text"  onChange={this.updateSearch.bind(this)} value={this.state.search}/> 
        </div>
        <table className="table">
          <thead>
            <tr> 
              <th>Name</th>
              <th>E-mail</th>
              <th>Phone</th>
              <th>City</th>
              <th>Note</th>
            </tr>
          </thead>
          <tbody>
            {contacts.map(c =>
              <tr key={c.id}>
                <td>
                  <Link to={ROUTES.getUrl(ROUTES.CONTACT_DETAIL, { id: c.id })}>{c.name}</Link>
                  </td>
                <td>{c.email}</td>
                <td>{c.phone}</td>
                <td>{c.city}</td>
                <td>{('' + c.note).slice(0,100)}</td>
              </tr>
            )}

          </tbody>
        </table>
        <div>
          <Link to={ROUTES.CONTACT_NEW} className="btn btn-light" > Create New </Link>
        </div>
      </div>
    )
  }
}



export default ContactListing