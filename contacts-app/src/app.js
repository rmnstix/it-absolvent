import React from 'react'
import { HashRouter, Route, Switch, NavLink } from 'react-router-dom'
import ContactListing from './contacts/contact-listing'
import ContactEdit from './contacts/contact-edit'
import ContactDetail from './contacts/contact-detail'
import Login from './login'
import authService from './auth-service'
import ROUTES from './routes'
import ContactNew from './contacts/contact-new'
import UserListing from './users/user-listing'
import UserEdit from './users/user-edit'
import UserNew from './users/user-new'
// let Page = Listing

// const goTo = (Comp) =>
//   Page = Comp

const App = () =>
  <HashRouter>
    <div>
      {!authService.isLoggedIn() && <Login />}
      {authService.isLoggedIn() &&
        <div>
          <Navigation />

          <div className="container-fluid">
            <Switch> 
              <Route path={ROUTES.USER_LISTING} component={UserListing} />
              <Route path={ROUTES.USER_EDIT} component={UserEdit} />
              <Route path={ROUTES.USER_NEW} component={UserNew} />
              <Route path={ROUTES.CONTACT_DETAIL} component={ContactDetail} />
              <Route path={ROUTES.CONTACT_EDIT} component={ContactEdit} />
              <Route path={ROUTES.CONTACT_NEW} component={ContactNew} />
              <Route path={ROUTES.CONTACT_LISTING} component={ContactListing} />
            </Switch>
          </div>
        </div>}
    </div>
  </HashRouter>

const Navigation = () =>
  <nav className="navbar navbar-expand-lg navbar-light bg-light">
    <div className="d-flex justify-content-between">
      <ul className="navbar-nav mr-auto">
        <li className="nav-item active">
          <NavLink exact className="nav-link" to={ROUTES.CONTACT_LISTING}>Contacts</NavLink>
        </li>
        <li className="nav-item active">
          <NavLink exact className="nav-link" to={ROUTES.USER_LISTING}>Users</NavLink>
        </li>
      </ul>

      <ul className="navbar-nav mr-auto">
        <li className="nav-item active">
          <a className="nav-link" onClick={() => authService.logout()}>Logout</a>
        </li>
      </ul>
    </div>
  </nav>

export default App;
