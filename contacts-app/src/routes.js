export default {
    CONTACT_LISTING: '/',
    CONTACT_DETAIL: '/detail/:id',
    CONTACT_EDIT: '/edit/:id',
    CONTACT_NEW: '/new',

    USER_LISTING: '/users',
    USER_EDIT: '/uedit/:id',
    USER_NEW: '/unew',
    
    getUrl(path, params = {}) {
      return path.replace(/\:(\w+)/g, (m, k) => params[k])
    }
}