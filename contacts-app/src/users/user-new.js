import React from 'react';
import { Link } from 'react-router-dom';
import ROUTES from '../routes'
import usersService from './user-service'
import UserForm from './user-form'


class UserNew extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      user: {}
    }
  }

  

  create() {
    usersService.createUser(this.state.user).then(res => {
      this.props.history.push(ROUTES.USER_LISTING)
    })
  }

  render() {
    const user = this.state.user

    return (
      <div>
        <h2> Create New</h2>
        <UserForm user={user} />
        <button type="submit" className="btn btn-primary" onClick={() => this.create()}>Create</button>
      </div>
    )
  }
}

export default UserNew