import React from 'react';
import { Link } from 'react-router-dom';
import ROUTES from '../routes'
import usersService from './user-service'
import UserForm from './user-form'


class UserEdit extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      user: null
    }
  }

  componentWillMount() {
    this.load(this.props.match.params.id)
  }

  componentWillReceiveProps(newProps) {
    if (this.props.match.params.id !== newProps.match.params.id) {
      this.load(newProps.match.params.id)
    }
  }

  load(id) {
    usersService.getUser(id).then(res => {
      this.setState({
        user: res.data
      })
    })
  }

  update() {
    usersService.updateUser(this.state.user).then(res => {
      this.props.history.push(ROUTES.USER_LISTING)
    })
  }

  render() {
    const user = this.state.user

    return (
      <div>
        <h2> Edit</h2>
        {user && <UserForm user={user} />}
        <button type="submit" className="btn btn-primary" onClick={() => this.update()}>Save</button>
      </div>
    )
  }
}



// const Edit = (props) => {
//   const contacts = contactsService.getContact(props.match.params.id)

//   const handleChange = (e) =>
//     contacts[e.target.name] = e.target.value

//   return (
//     <div>
//       <h2> Edit</h2>
//       <form>
//         <div className="form-row">
//           <div className="form-group col-md-4">
//             <label className="col-form-label">Name</label>
//             <input name="name" type="text" className="form-control" value={contacts.name} onChange={handleChange} />
//           </div>
//           <div className="form-group col-md-4">
//             <label >Email</label>
//             <input name="email" type="email" className="form-control" placeholder="Email" value={contacts.email} onChange={handleChange} />
//           </div>
//           <div className="form-group col-md-4">
//             <label >Phone</label>
//             <input name="phone" type="text" className="form-control" value={contacts.phone} onChange={handleChange} />
//           </div>
//         </div>
//         <div className="form-group">
//           <label >Note</label>
//           <textarea name="note" type="text" className="form-control" value={contacts.note} onChange={handleChange} />
//         </div>
//         <Link type="submit" className="btn btn-primary" to={ROUTES.LISTING}>Save</Link>
//       </form>
//     </div>)
// }



export default UserEdit 