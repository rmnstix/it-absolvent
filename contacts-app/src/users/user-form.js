import React from 'react'

const UserForm = ({ user }) => {
  const handleChange = (e) =>
    user[e.target.name] = e.target.value

  return (
    <form> 
      <div className="form-row">
        <div className="form-group col-md-4">
          <label className="col-form-label">Name</label>
          <input name="name" type="text" className="form-control" value={user.name} onChange={handleChange} />
        </div>
        <div className="form-group col-md-4">
          <label >Login</label>
          <input name="login" type="text" className="form-control" value={user.login} onChange={handleChange} />
        </div>
        </div>
        {/* <div className="form-group col-md-4">
          <label >Phone</label>
          <input name="phone" type="text" className="form-control" value={user.phone} onChange={handleChange} />
        </div>
      </div>
      <div className="form-group">
        <label >Note</label>
        <textarea name="note" type="text" className="form-control" value={user.note} onChange={handleChange} />
      </div> */}
    </form>
  )
}

export default UserForm