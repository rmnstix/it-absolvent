import React from 'react';
import { Link } from 'react-router-dom';
import ROUTES from '../routes'
// import Detail from './Detail';
//asdkk

import usersService from './user-service'

class UserListing extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      users: []
    }
  }
š
  componentWillMount() {
    this.load()
  }

  load() {
    usersService.getUsers().then(res => {
      this.setState({
        users: res.data
      })
    })
  }

  render() {
    const users = this.state.users

    return (
      <div>
        <table className="table">
          <thead>
            <tr> 
              <th>Login</th>
              <th>Name</th>
              <th>Phone</th>
              <th>Note</th>
            </tr>
          </thead>
          <tbody>
            {users.map(u =>
              <tr key={u.id}>
                <td>
                  <Link to={ROUTES.getUrl(ROUTES.USER_EDIT, { id: u.id })}>{u.login}</Link>
                  </td>
                <td>{u.name}</td>
              </tr>
            )}

          </tbody>
        </table>
        <div>
          <Link to={ROUTES.USER_NEW} className="btn btn-light" > Create New </Link>
        </div>
      </div>
    )
  }
}



export default UserListing