import axios from 'axios'

export default {
  getUsers() {
    return axios.get('http://localhost:3000/users')
  },

  createUser(data) {
    return axios.post('http://localhost:3000/users', data)
  },

  getUser(id) {
    return axios.get('http://localhost:3000/users/' + id)
  }, 

  updateUser(user) {
    return axios.put('http://localhost:3000/users/' + user.id, user)
  }
}