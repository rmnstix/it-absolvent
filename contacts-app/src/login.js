import React from 'react'
import authService from './auth-service'

const Login = () =>
    <div className="container-fluid">
        <form>
            <h2>Please login</h2>
            <input type="text" className="form-control" name="username" placeholder="Email Address" required="" />
            <input type="password" className="form-control" name="password" placeholder="Password" required="" />
            <label className="checkbox">
                <input type="checkbox" value="remember-me"  name="rememberMe" /> Remember me
            </label>
                <button className="btn btn-lg btn-primary btn-block" type="submit" onClick={() => authService.login()}>Login</button>   
        </form>
    </div>

export default Login