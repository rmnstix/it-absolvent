import axios from 'axios'

export default {
  user: null,

  onChange: null,

  async init(){
    this.reload()
  },

  async reload(){
    try {
      const res = await axios.get('http://localhost:3000/user')
      this.user = res.data     
    }catch(e) {
      this.user = null
    }
    this.onChange()
  },

  isLoggedIn(){
    return this.user !== null
  },

  login(){
    return axios.post('http://localhost:3000/login')
    this.reload()
  },

  logout(){
    return axios.post('http://localhost:3000/logout').then(() => this.onChange())
  }
}

// let loggedIn = false

// export const isLoggedIn = () => {
//         return loggedIn
//     }

// export const login = () => {
//         loggedIn = true
//     }

// export const logout = () => {
//         loggedIn = false 
//     }

